/* global L, distance */
var baseUrl = 'https://rest.ehrscape.com/rest/v1';
var queryUrl = baseUrl + '/query';

var username = "ois.seminar";
var password = "ois4fri";
var LAT = 46.05004;
var LNG = 14.46931;
var mapa;
var podatkiGraf = [];
var poligoni = [];
var krogi = [];
var radij = 1000;


/**
 * Generiranje niza za avtorizacijo na podlagi uporabniškega imena in gesla,
 * ki je šifriran v niz oblike Base64
 *
 * @return avtorizacijski niz za dostop do funkcionalnost
 */
function getAuthorization() {
  return "Basic " + btoa(username + ":" + password);
}


/**
 * Generator podatkov za novega pacienta, ki bo uporabljal aplikacijo. Pri
 * generiranju podatkov je potrebno najprej kreirati novega pacienta z
 * določenimi osebnimi podatki (ime, priimek in datum rojstva) ter za njega
 * shraniti nekaj podatkov o vitalnih znakih.
 * @param stPacienta zaporedna številka pacienta (1, 2 ali 3)
 * @return ehrId generiranega pacienta
 */
 
 
 // pripravimo 3 paciente 
  var pacienti =[{"stPacienta":"1","ime": "Larry", "priimek": "Wheels", "datumRojstva": "1980-11-11", "visina": "200", "teza": "140", "ehrId": "0", "datumInUra": "2000-05-08T11:40:00.000Z",
  "sistolicniKrvniTlak": "0", "diastolicniKrvniTlak": "0", "nasicenostKrviSKisikom" : "0" },
  {"stPacienta":"2","ime": "John", "priimek": "Heck", "datumRojstva": "1972-01-10", "visina": "160", "teza": "60","ehrId": "0", datum : "1990-05-08T11:40:00.000Z",
  "sistolicniKrvniTlak": "0", "diastolicniKrvniTlak": "0", "nasicenostKrviSKisikom" : "0" },
  {"stPacienta":"3","ime": "Stefanie", "priimek": "Cohen", "datumRojstva": "1999-02-02", "visina": "150", "teza": "30","ehrId": "0", datum: "2000-05-08T11:40:00.000Z",
  "sistolicniKrvniTlak": "0", "diastolicniKrvniTlak": "0", "nasicenostKrviSKisikom" : "0" }] ;

  
  function generirajPodatke(stPacienta) {
      
      var ime = pacienti[stPacienta].ime;
      var priimek = pacienti[stPacienta].priimek;
      var datumRojstva = pacienti[stPacienta].datumRojstva;
      var telesnaVisina = pacienti[stPacienta].visina;
      var telesnaTeza = pacienti[stPacienta].teza;
      var ehrId = pacienti[stPacienta].ehrId;
      var datumInUra = pacienti[stPacienta].datumInUra;
      var sistolicniKrvniTlak = pacienti[stPacienta].sistolicniKrvniTlak;
      var diastolicniKrvniTlak = pacienti[stPacienta].diastolicniKrvniTlak;
      var nasicenostKrviSKisikom = pacienti[stPacienta].nasicenostKrviSKisikom;
      var merilec = "Zure";
      
      var podatki = {
			// Struktura predloge je na voljo na naslednjem spletnem naslovu:
      // https://rest.ehrscape.com/rest/v1/template/Vital%20Signs/example
		    "ctx/language": "en",
		    "ctx/territory": "SI",
		    "ctx/time": datumInUra,
		    "vital_signs/height_length/any_event/body_height_length": telesnaVisina,
		    "vital_signs/body_weight/any_event/body_weight": telesnaTeza,
		};
      
      // ustvarimo EHR id za nasega pacienta in vnesemo podatke v podatkovno bazo EHR
      $.ajax({
      url: baseUrl + "/ehr",
      type: 'POST',
      headers: {
        "Authorization": getAuthorization()
      },
      success: function (data) {
        ehrId = data.ehrId;
        var partyData = {
          firstNames: ime,
          lastNames: priimek,
          dateOfBirth: datumRojstva,
          additionalInfo: {"ehrId": ehrId}
        };
         $.ajax({
          url: baseUrl + "/demographics/party",
          type: 'POST',
          headers: {
            "Authorization": getAuthorization()
          },
          contentType: 'application/json',
          data: JSON.stringify(partyData),
         })
        
        pacienti[stPacienta].ehrId = ehrId;
        $("#preberiEHRid").val(ehrId);
        
        var parametriZahteve = {
		    ehrId: $("#preberiEHRid").val(),
		    templateId: 'Vital Signs',
		    format: 'FLAT',
		    committer: merilec
		    };
		    
		    $.ajax({
      url: baseUrl + "/composition?" + $.param(parametriZahteve),
      type: 'POST',
      contentType: 'application/json',
      data: JSON.stringify(podatki),
      headers: {
        "Authorization": getAuthorization()
      }
		});
        
       
        
        var partyData = {
          firstNames: ime,
          lastNames: priimek,
          dateOfBirth: datumRojstva,
          additionalInfo: {"ehrId": ehrId}
        };
      }
		});
	
      
  }
  
  
  
  function generirajVseTri () {
    for (var i = 0 ; i < 3 ; i ++) {
      generirajPodatke(i);
      
    }
  }
  
  
  
  
  function izracunajBMI () {
    $("#rezultatVitalni").html("")
    var ehrId = $("#preberiEHRid").val();
    var teza;
    var visina = [];
    
    $.ajax({
			url: baseUrl + "/demographics/ehr/" + ehrId + "/party",
	    	type: 'GET',
	    	headers: {
          "Authorization": getAuthorization()
        },
          success: function (data) {
            var party = data.party;
            var ime = party.firstNames;
            var priimek = party.lastNames;
            // tu lahko izpises se ime priimek itd...
            
            
            $.ajax({
              url: baseUrl + "/view/" + ehrId + "/" + "height",
              type: 'GET',
              headers: {
                "Authorization": getAuthorization()
              },
              success: function (rezulti) {
                for (var i in rezulti) {
                  visina[i] = rezulti[i].height/100;
                }
                
                $.ajax({
    				  url: baseUrl + "/view/" + ehrId + "/" + "weight",
    			    type: 'GET',
    			    headers: {
                "Authorization": getAuthorization()
                },
                
                success: function (res) {
                  var results = "<table class='table table-striped " +
                    "table-hover'><tr><th>Datum in ura tehtanja (" + ime + " " + priimek + ") </th>" +
                    "<th class='text-center'>BMI</th></tr>";
                    var counter = 0;
                    for (var i in res) {
                      ++ counter;
                    }
                    
                  
                  var data = new Array(counter + 1);
                  data[0] = new Array(4);
                  data[0][0] = 'Datum ';
                  data[0][1] = 'VAS-BMI';
                  data[0][2] = 'MEJA-NORM';
                  data[0][3] = 'MEJA-NORM';
                  var a = res.length;
  				        for (var i in res) {
  				          
  				          data[a] = new Array(4);
  				          var bmi = Math.round (res[i].weight / (visina[i]*visina[i]) * 100) / 100;
  				          var year = parseInt(res[i].time);
  				          
  				          data[a][0] = year.toString();
  				          
  				          data[a][1] = bmi;
  				          data[a][2] = 24.9;
  				          data[a][3] = 18.5;
  				          var barva = 'green';
  				          var stanje = '0';
  				          if (bmi <18.5) {
  				            stanje = "Podharnjenost";
  				          }
  				          if (bmi >= 18.5 && bmi <= 25) {
  				            stanje = "Normalna hranjenost";
  				          }
  				          if (bmi < 18.5 || bmi > 25) {
  				            barva = 'red';
  				          }
  				          if (bmi >= 25) {
  				            stanje = "Prekomerna telesna masa";
  				          }
  				          var html = '<font face="verdana" color = "' + barva + '">';
    		            results += "<tr><td><class= 'text-left'>" + res[i].time +
                      "</td><td class='text-right'>" + html +  bmi + " (" + stanje + ")"  +  "</font> " + "</td></tr>";
                      
                      --a;
  				        }
  				        
  				        podatkiGraf = data;
  				        results += "</table>";
  				        $("#rezultatVitalni").append(results);
  				        
  				        // tu je na razpolago bmi -- narisemo se graf
  				        
  				        
                }
              
            })
                
                
              }
            })
          }
    })   
    
  }
  
  
  
  function kreirajEHRzaBolnika() {
      var ime = $("#kreirajIme").val();
	    var priimek = $("#kreirajPriimek").val();
      var datumRojstva = $("#kreirajDatumRojstva").val();
      
      if (!ime ||!priimek ||!datumRojstva|| ime.trim().length == 0 || priimek.trim().length == 0
      || datumRojstva.trim().length == 0) {
        $("#novEHR").html("<span class = 'label-warning fade-in'> Prislo je do napake preverite vnesene podatke ! </span>");
      }else {
      
      $.ajax({
        url: baseUrl + "/ehr",
        type: 'POST',
        headers: {
          "Authorization":getAuthorization()
        },
        
        success: function (data) {
          var ehrId = data.ehrId;
          var partyData = {
            firstNames: ime,
            lastNames: priimek,
            dateOfBirth: datumRojstva,
            additionalInfo: {"ehrId": ehrId}
          };
          
          $.ajax({
            url:baseUrl + "/demographics/party",
            type: 'POST',
            headers: {
              "Authorization": getAuthorization()
            },
            contentType: 'application/json',
            data: JSON.stringify(partyData),
            
            success: function (party) {
              if (party.action == 'CREATE') {
                $("#novEHR").html(" Vaš EHR-id je bil uspešno kreiran:" + "<span class = ' label label-success fade-in'>" + ehrId + "</span>" );
              }else {
                $("#novEHR").html("Prišlo je do napake, preverite vnešene podatke !")
              }
            }
          })
        }
        
      })
      
      }
  }
  
  function dodajMeritveVitalnihZnakov() {
	var ehrId = $("#dodajVitalnoEHR").val();
	var datumInUra = $("#dodajVitalnoDatumInUra").val();
	var telesnaVisina = $("#dodajVitalnoTelesnaVisina").val();
	var telesnaTeza = $("#dodajVitalnoTelesnaTeza").val();
	var merilec = 'Bostjan Fjavz' ;

	if (!ehrId || ehrId.trim().length == 0) {
		$("#dodajMeritveVitalnihZnakovSporocilo").html("<span class='obvestilo " +
      "label label-warning fade-in'>Prosim vnesite zahtevane podatke!</span>");
	} else {
		var podatki = {
			// Struktura predloge je na voljo na naslednjem spletnem naslovu:
      // https://rest.ehrscape.com/rest/v1/template/Vital%20Signs/example
		    "ctx/language": "en",
		    "ctx/territory": "SI",
		    "ctx/time": datumInUra,
		    "vital_signs/height_length/any_event/body_height_length": telesnaVisina,
		    "vital_signs/body_weight/any_event/body_weight": telesnaTeza,
		};
		var parametriZahteve = {
		    ehrId: ehrId,
		    templateId: 'Vital Signs',
		    format: 'FLAT',
		    committer: merilec
		};
		$.ajax({
      url: baseUrl + "/composition?" + $.param(parametriZahteve),
      type: 'POST',
      contentType: 'application/json',
      data: JSON.stringify(podatki),
      headers: {
        "Authorization": getAuthorization()
      }
		});
	}
}

function posodbiRestavracije () {
    $.getJSON("knjiznice/json/kalorije.json", function(data) {
      $("#seznamRestavracij").html("")
        
        var results = '<table style="width:50%" align="center"><tr><th></th></tr>';
        for (var i in data) {
          results = results + "<tr> <td>" + "<button type='button' class='btn btn-primary btn-xs' onclick= posodbiKalorije("+i+ ")>" + data[i].restaurant + "</button>" + "</td></tr>"   ;
        }
        results = results + '</table>';
       $('#seznamRestavracij').append (results);
    });
  }
function posodbiKalorije (x) {
  $.getJSON("knjiznice/json/kalorije.json", function (data){
    $("#seznamHrane").html("")
    var restavracija = data[x];
    
    var result = "";
    for (var i in restavracija.foodItems ) {
      result = result + "<option>"+restavracija.foodItems[i].foodName +"(" + restavracija.foodItems[i].calories+ " kcal)" +"</option>"
      
    }
    
    $('#seznamHrane').append (result);
  })
}  


  
  
  
  

$(document).ready(function() {
  
  
  
  
  
  /**
   * Napolni testni EHR ID pri prebiranju EHR zapisa obstoječega bolnika,
   * ko uporabnik izbere vrednost iz padajočega menuja
   */
  var stevec = 0;
	$('#preberiObstojeciEHR').change(function() {
		$("#preberiSporocilo").html("");
		var a = pacienti[0].ehrId;
		var b = pacienti[1].ehrId;
		var c = pacienti[2].ehrId;
		var vrednost = $(this).val();
		
		
		if (vrednost == 1) {
		  $("#preberiEHRid").val(a);
		}
		if (vrednost == 2) {
		  $("#preberiEHRid").val(b);
		}
		if (vrednost == 3) {
		  $("#preberiEHRid").val(c);
		}
		
	});
	
	function obKlikuNaMapo() {
    var staticUrl = 'https://teaching.lavbic.net/cdn/OIS/DN3/bolnisnice.json';
    $.getJSON(staticUrl, function(data) {
    
    
    
    
    for (var i= 1; i < poligoni.length; i++) {
        mapa.removeLayer(poligoni[i]);  
      }
     for (var i= 1; i < krogi.length; i++) {
        mapa.removeLayer(krogi[i]);  
      }  
      
      
    
    for (var i = 0; i <data.features.length; i ++){
      
      var tip = data.features[i].geometry.type;
      var latlngs = data.features[i].geometry.coordinates;
      
      
      if (data.features[i].properties.type == "multipolygon") {
        //console.log (data.features[i].geometry.coordinates);
        var temp = data.features[i].geometry.coordinates;
        var lastnosti = data.features[i].properties;
        var barva = 'green';
       
        
        for (var j = 0 ; j < temp.length ; j ++ ) {
          var jeBlizu = true;
          for (var z = 0; z < temp[j].length ; ++ z) {
            var x = temp[j][z][0];
            var y = temp[j][z][1];
            temp[j][z][1] = x;
            temp[j][z][0] = y;
            if (distance(y, x, LAT, LNG, "K") > (radij/1000) ) {
                jeBlizu = false;
            }
            
           // console.log (temp[j][z][0] + "   " +temp[j][z][1] )
          }
         // console.log (temp);
         if (jeBlizu == false){
           barva = 'blue'
         }
          var polygon = L.polygon(temp, {color: barva}).bindPopup(lastnosti.name + "          " + lastnosti['addr:street'] + " " + lastnosti['addr:housenumber']).addTo(mapa);
          poligoni.push(polygon);
        }
       // console.log (temp[0][0][0] + " ----------------" +temp[0][0][1] )
      //  console.log (temp);
      }
      
      if (tip == "Polygon" && data.features[i].properties.type != "multipolygon") {
        
        var jeBlizu = true;
        var barva = 'green';
        var lastnosti = data.features[i].properties;
        for (var j = 0 ; j < latlngs.length ; j ++){
          for (var z = 0 ; z < latlngs[j].length ; z ++) {
            var x = latlngs[j][z][0];
            var y = latlngs[j][z][1];
            latlngs[j][z][1] = x;
            latlngs[j][z][0] = y;
            if (distance(y, x, LAT, LNG, "K") > (radij/1000) ) {
                jeBlizu = false;
            }

          }
        }
       //console.log (latlngs);
       
       if (jeBlizu == false) {
         barva = 'blue';
       }
       var polygon = L.polygon(latlngs, {color: barva}).bindPopup(lastnosti.name + "          " + lastnosti['addr:street'] + " " + lastnosti['addr:housenumber']).addTo(mapa);
       poligoni.push(polygon);
      }
      
      if (tip == "Point") {
        var lastnosti = data.features[i].properties;
        var jeBlizu = true;
        var barva = 'green';
        
        var x = latlngs[0];
        var y = latlngs[1];
        if (distance(y, x, LAT, LNG, "K") > (radij/1000) ) {
           jeBlizu = false;
           barva = 'blue';
        }
        latlngs[0] = y;
        latlngs[1] = x;
       var krog = L.circle (latlngs,{
          color: barva,
          fillColor:barva,
          opacity: .70,
          radius: 10
        }).bindPopup(lastnosti.name + "          " + lastnosti['addr:street'] + " " + lastnosti['addr:housenumber'] ).addTo(mapa);
        krogi.push(krog);
       /* var popup = L.popup()
    .setLatLng(latlngs)
    .setContent("I am a standalone popup.")
    .openOn(mapa); */
      }
   
  }
});
  }
  
  $('#grafIzrc').click(function() {
	    
	    google.charts.load('current', {'packages':['corechart']});
      google.charts.setOnLoadCallback(drawChart);
      
      function drawChart () {
        var data = google.visualization.arrayToDataTable (podatkiGraf);
                  
      var options = {
        title: 'Vas BMI skozi zgodovino tehtanja',
        curveType: 'function',
        legend: { position: 'bottom' }
      };
    
    var chart = new google.visualization.LineChart(document.getElementById('curve_chart'));
    chart.draw(data, options);
    
  }
	})
  ////
      
      var podatkiZemljevida = new Array(196);
      //var staticUrl = 'https://apps.who.int/gho/athena/data/GHO/NCD_BMI_MEANC.json?profile=simple&filter=SEX:*;COUNTRY:*;AGEGROUP:YEARS18-PLUS';
      var url = 'https://apps.who.int/gho/athena/data/GHO/NCD_BMI_MEANC.json?profile=simple&filter=SEX:*;COUNTRY:*;AGEGROUP:YEARS05-09';
      
  //API KLIC WHO 
  var baseUrl = 'https://apps.who.int/gho/athena/data/GHO/NCD_BMI_MEANC.json?profile=simple&filter=SEX:*;COUNTRY:*;AGEGROUP:YEARS05-09' ;
  $.ajax({
      url: baseUrl,
      type: 'GET',
      dataType: "jsonp",
      success: function (data){
        podatkiZemljevida[0] = new Array(2);
        podatkiZemljevida[0][0] = 'Drzava';
        podatkiZemljevida[0][1] = 'Bmi';
        var counter = 1;
        for (var i = 0 ; i < data.fact.length ; i ++) {
         if (data.fact[i].dim.YEAR == '2016' && data.fact[i].dim.SEX == "Both sexes"){
           podatkiZemljevida[counter]  = new Array(2);
           podatkiZemljevida[counter][0] = data.fact[i].dim.COUNTRY;
           podatkiZemljevida[counter][1] = parseInt (data.fact[i].Value);
           if (isNaN (podatkiZemljevida[counter][1]) == true) {
             podatkiZemljevida[counter][1] = 0;
           }
           ++ counter;
         }
        }
        //console.log(podatkiZemljevida);
        
      google.charts.load('current', {
        'packages':['geochart'],
        // Note: you will need to get a mapsApiKey for your project.
        // See: https://developers.google.com/chart/interactive/docs/basic_load_libs#load-settings
        'mapsApiKey': 'AIzaSyD-9tSrke72PouQMnMX-a7eZSW0jkFMBWY'
      });  
    
      if (counter == 196) {
      
      $('#narisiMapo').click(function(){
        google.charts.setOnLoadCallback(drawRegionsMap());
      })
      }
      
      function drawRegionsMap() {
        
       
        
        var data = google.visualization.arrayToDataTable (podatkiZemljevida);

        var options = {};

        var chart = new google.visualization.GeoChart(document.getElementById('regions_div'));

        chart.draw(data, options);
      }
      }
  });
        
        //console.log (podatkiZemljevida);
	/////
	//getLocation();
  
  
  
  var mapOptions = {
    zoom: 20,
    center: [LAT, LNG],
    // maxZoom: 3
  };

  // Ustvarimo objekt mapa
 mapa = new L.map('mapa_id', mapOptions);

  // Ustvarimo prikazni sloj mape
  var layer = new L.TileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png');
  
  // Prikazni sloj dodamo na mapo
  mapa.addLayer(layer);
  L.circle ([LAT,LNG],{
          color: 'red',
          fillColor:'green',
          opacity: .50,
          radius: 20
        }).addTo(mapa);
  ++ stevec;
  
	

	function kliknemoNaMapo(e) {
    var latlng = e.latlng;
    
    LAT= latlng.lat;
    LNG = latlng.lng;
    
    obKlikuNaMapo();
  }

  mapa.on('click', kliknemoNaMapo);
	
  
	

});
